FROM php:8.1-cli-buster

COPY --from=composer:2.1 /usr/bin/composer /usr/bin/composer

RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		git \
		wget \
		curl \
		gzip \
		libzip-dev \
		unzip \
	; \
	docker-php-ext-install -j "$(nproc)" zip ; \
	\
	rm -rf /var/lib/apt/lists/*

ENTRYPOINT []
CMD []
